# syntax = docker/dockerfile:1.0-experimental
ARG USER_NAME=fk
ARG USER_ID=8888
ARG HOME=/app
ARG VIRTUAL_ENV=${HOME}/fk_venv
ARG BUILD_DATE
ARG VCS_REF
ARG BUILD_VERSION

################################################################################
################################################################################
FROM python:3.11-slim as python-dev
################################################################################

ARG USER_NAME
ARG USER_ID
ARG HOME
ARG VIRTUAL_ENV
ARG VCS_REF
ARG BUILD_VERSION


RUN apt-get update && apt-get install -y --no-install-recommends \
	make \
	curl \
	file \
	gnupg2 \
	apt-transport-https \
	debconf-utils \
	nano \
# For top and ps commands
	procps \
	locales \
# For general python wheel builds
	build-essential \
	python3-dev \
# For pg_config as requirement of psycopg2
	libpq-dev \
	python3-pip \
# For bzip
	libbz2-dev \
# For bjoern WSGI package
	libev-dev \
 && apt-get autoremove -y \
 && apt-get clean \
 && locale-gen en_US.UTF-8 \
 && update-locale \
 && rm -rf /var/lib/apt/lists/*

# https://docs.gitlab.com/omnibus/update/package_signatures#package-repository-metadata-signing-keys
RUN grep 'deb \[signed-by=' /etc/apt/sources.list.d/gitlab_gitlab-?e.list || true
RUN apt-key del 3F01618A51312F3F
RUN curl -s https://packages.gitlab.com/gpg.key | apt-key add -
RUN apt-key list 3F01618A51312F3F

RUN which pg_config

ENV USER_NAME ${USER_NAME}
ENV USER_ID ${USER_ID}
ENV HOME ${HOME}
ENV VIRTUAL_ENV ${VIRTUAL_ENV}

RUN echo "USER_NAME=  ${USER_NAME}"; \
	echo "USER_ID=    ${USER_ID}"; \
	echo "HOME=       ${HOME}"; \
	echo "VIRTUAL_ENV=${VIRTUAL_ENV}"

RUN mkdir -p "${HOME}"

# Only tested on alpine from https://stackoverflow.com/questions/49955097/how-do-i-add-a-user-when-im-using-alpine-as-a-base-image
RUN addgroup \
	--gid "${USER_ID}" \
	--system \
	"${USER_NAME}"

RUN adduser \
	--disabled-password \
	--gecos "" \
	--home "${HOME}" \
	--ingroup "${USER_NAME}" \
	--shell "/sbin/nologin" \
	--no-create-home \
	--uid "${USER_ID}" \
	"${USER_NAME}"


#RUN groupadd -r -g "$USER_ID" "$USER_NAME"
#RUN useradd -r -u "$USER_ID" -g "$USER_NAME" -d "$HOME" -s /sbin/nologin -c "$USER_NAME" "$USER_NAME"
RUN chown -R "${USER_ID}:${USER_ID}" "${HOME}"
USER $USER_ID
WORKDIR $HOME
RUN python -m venv $VIRTUAL_ENV
ENV PATH="${VIRTUAL_ENV}/bin:${HOME}/.local/bin:${PATH}"


COPY --chown=8888:8888 requirements/requirements.txt requirements.txt
#${USER_ID}:${USER_ID}

RUN pip install --upgrade pip \
 && pip install --upgrade setuptools setuptools-scm wheel pip-tools \
 && pip install --upgrade -r requirements.txt \
 && pip freeze

RUN file /usr/lib/x86_64-linux-gnu/libbz2*


################################################################################
################################################################################
FROM python:3.11-slim as python-build
################################################################################

ARG USER_NAME
ARG USER_ID
ARG HOME
ARG VIRTUAL_ENV
ARG VCS_REF
ARG BUILD_VERSION

RUN echo "############################ PYTHON-BUILD"

# get gcc for wheel builds
RUN apt-get update && apt-get install -y --no-install-recommends \
	gcc \
	libc6-dev \
	libpq-dev \
 && apt-get autoremove -y \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \

ENV USER_NAME ${USER_NAME}
ENV USER_ID ${USER_ID}
ENV HOME ${HOME}
ENV VIRTUAL_ENV ${VIRTUAL_ENV}

RUN mkdir -p "${HOME}"

# Only tested on alpine from https://stackoverflow.com/questions/49955097/how-do-i-add-a-user-when-im-using-alpine-as-a-base-image
RUN addgroup \
	--gid "${USER_ID}" \
	--system \
	"${USER_NAME}"

RUN adduser \
	--disabled-password \
	--gecos "" \
	--home "${HOME}" \
	--ingroup "${USER_NAME}" \
	--shell "/sbin/nologin" \
	--no-create-home \
	--uid "${USER_ID}" \
	"${USER_NAME}"

#RUN groupadd -r -g "$USER_ID" "$USER_NAME"
#RUN useradd -r -u "$USER_ID" -g "$USER_NAME" -d "$HOME" -s /sbin/nologin -c "$USER_NAME" "$USER_NAME"
RUN chown -R "${USER_ID}:${USER_ID}" "${HOME}"
USER $USER_ID
WORKDIR $HOME

RUN cat /etc/debian_version

# Get libs
COPY --from=python-dev \
	/usr/lib/x86_64-linux-gnu/libpq.* \
	/usr/lib/x86_64-linux-gnu/libgss* \
	/usr/lib/x86_64-linux-gnu/libldap* \
	/usr/lib/x86_64-linux-gnu/libkrb5* \
	/usr/lib/x86_64-linux-gnu/libk5* \
	/usr/lib/x86_64-linux-gnu/libssl* \
	/usr/lib/x86_64-linux-gnu/liblber* \
	/usr/lib/x86_64-linux-gnu/libsasl* \
	/lib/x86_64-linux-gnu/libkeyutils* \
	/usr/lib/x86_64-linux-gnu/libev* \
	/usr/lib/x86_64-linux-gnu/

# Get headers
COPY --from=python-dev \
	/usr/include/* \
	/usr/include/

# Get some tools
COPY --from=python-dev \
	/bin/nano \
	/bin/ps \
	/usr/bin/top \
	/usr/bin/pg_config \
	/usr/bin/

# Get main app's venv
COPY --from=python-dev --chown=8888:8888 ${VIRTUAL_ENV} ${VIRTUAL_ENV}
#${USER_ID}:${USER_ID} 

ENV PATH="${VIRTUAL_ENV}/bin:${HOME}/.local/bin:${PATH}"

# Do nothing for a long time. This is meant to be overridden
CMD ["sleep", "${USER_ID}"]


# https://medium.com/@chamilad/lets-make-your-docker-image-better-than-90-of-existing-ones-8b1e5de950d
LABEL maintainer="Lennart Rolland <lennart@merchbot.net>"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.name="octomy/base-py"
LABEL org.label-schema.description="Base iamge for all python based services"
LABEL org.label-schema.url="http://merchbot.net"
LABEL org.label-schema.vcs-url="https://gitlab.com/octomy/base-py"
LABEL org.label-schema.vendor="merchbot"


# https://pythonspeed.com/articles/faster-multi-stage-builds/
# https://pythonspeed.com/articles/multi-stage-docker-python/
# https://pythonspeed.com/articles/activate-virtualenv-dockerfile/

