[![pipeline status](https://gitlab.com/octomy/base-py/badges/production/pipeline.svg)](https://gitlab.com/octomy/base-py/-/commits/production)

# About base-py

<img src="https://gitlab.com/octomy/base-py/-/raw/production/design/logo-1024.png" width="20%"/>

Docker image that serves as a base for all other octomy images that need python

- base-py is [available on gitlab](https://gitlab.com/octomy/base-py).
- base-py is [available in gitlab container registry](https://gitlab.com/octomy/base-py/container_registry).

```shell
# Clone git repository
git clone git@gitlab.com:octomy/base-py.git

```

```shell
# Pull from container registry
docker pull registry.gitlab.com/octomy/base-py

```


# Note on version

The version number of this image is maintained in the VERSION file in the root of the project.

It will mirror the Python version inside such that 3.11.0 is latest Python 3.11 at the time with 0 being base-py revision


